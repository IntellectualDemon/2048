/**
 * @author Intellectual Demon <intellectualdemon@protonmail.ch>
 **/

// Utilities
//   $ enhances querySelectorAll
function $(sel, node, a) { return (a = [].slice.call( (node || document).querySelectorAll(sel) )).length > 1 ? a : a[0] }

//   addEvents enhances addEventListener
function addEvents(obj) {
  function add(n) { n.addEventListener(es[i], obj[q][e].bind(n), false) }
  for (var q in obj) for (var e in obj[q]) {
    var ns = q ? $(q) : [window, document], es = e.split(" "), el = es.length, i = 0;
    for (; i < el; i++) typeof ns === "undefined" ? 0 : ns.constructor.name === "Array" ? ns.forEach(add) : add(ns)
  }
}

//   indexedDB interface
function Drum () {
  var req = indexedDB.open("2048", 1), drum = this;
  Object.defineProperty(this, "player", {
    get: function () {
      var tx = this._db.transaction(["player"], "readonly"),
          store = tx.objectStore("player");
      return new Promise (function (resolve, reject) {
        store.get("").onsuccess = function (e) {
          return resolve(e.target.result)
        }
      })
    },
    set: function (player) {
      var tx = this._db.transaction(["player"], "readwrite"),
          store = tx.objectStore("player");
      return new Promise (function (resolve, reject) {
        store.add(player, "");
        return resolve(player)
      })
    }
  });
  return new Promise(function (resolve, reject) {
    req.onupgradeneeded = function (e) {
      drum._db = e.target.result;
      var highscores = drum._db.createObjectStore("highscores", {autoIncrement: true});
      highscores.createIndex("score", "score", {unique: false});
      var player = drum._db.createObjectStore("player", {autoIncrement: false});
    };
    req.onsuccess = function (e) { drum._db = e.target.result; resolve(drum) };
    req.onerror = function (e) { return reject(true) }
  })
}
Drum.prototype._db = null;
Drum.prototype.getVals = function () {
  var tx = this._db.transaction("highscores", "readwrite"),
      store = tx.objectStore("highscores"), vals = [];
  return new Promise(function (resolve) {
    store.index("score").openCursor(null, "prev").onsuccess = function (e) {
      var csr = e.target.result;
      csr ? vals.push(csr.value) : resolve(vals);
      vals.length < 5 || resolve(vals);
      csr && csr.continue()
    }
  })
};
Drum.prototype.appendVal = function (val) {
  var tx = this._db.transaction(["highscores"], "readwrite"),
      store = tx.objectStore("highscores");
  return new Promise (function (resolve) {
    store.add(val);
    return resolve(val)
  })
};
Drum.prototype.purge = function () {
  indexedDB.deleteDatabase("2048");
  delete window.drum
};
Drum.prototype.constructor = Drum;

//   WebSocket interface
var wsuri = "wss://" + location.hostname + (location.port ? ":" + location.port : "") + "/";
function get_ws() {
  return new Promise(function (resolve, reject) {
    var ws = new WebSocket(wsuri);
    ws.onopen = resolve;
    ws.onerror = reject;
    ws.onmessage = function(m) {
      var data = JSON.parse(m.data);
      if ("id" in data) {
        drum.player = data;
        updateHS()
      } else if ("error" in data) {
        console.log(data.error);
        newDrum().then(function () { ws.send(JSON.stringify({msg: "idrequest", name: "test"})) })
      } else if ("highscores" in data) {
        var scoreText = "", val = data.highscores;
        if (val.length == 0) val = [{score: 0, moves: 0}];
        for (var i = 0; i < val.length; i++) {
          scoreText += val[i].score + "(" + val[i].moves + ")<br>"
        }
        $("span#highscores").innerHTML = scoreText}
    }
  })
}

// Game updater
function newDrum() {
  ("drum" in window) && drum.purge();
  return new Drum().then(function (drum) {
    window.drum = drum;
    return updateHS()
  })
}
newDrum().then(get_ws).then(function (e) {
  if (e) window.ws = e.target
}).then(function() {return drum.player}).then(function (player) {
  if (ws) ws.send(JSON.stringify({msg: "id", id: player.id}))
}).catch(function(e){
  if ("ws" in window) {
    // New player get name
    ws.send(JSON.stringify({msg: "idrequest", name: "test"}))
  }});

var boardState = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]],
    testState = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]],
    score = 0, moves = 0, gameover = false, touch = [], touchTimestamp = 0;

function updateHS() {
  drum.getVals().then(function (val) {
    var scoreText = "";
    if (val.length == 0) val = [{score: 0, moves: 0}];
    for (var i = 0; i < val.length; i++) {
      scoreText += val[i].score + "(" + val[i].moves + ")<br>"
    }
    $("span#pbs").innerHTML = scoreText
  }).catch(console.log)
}

function updateBoard() {
  $("span#score").textContent = score;
  $("span#moves").textContent = moves;
  var empty = [], i, j, k, len, vunit = window.innerWidth / window.innerHeight > 1 ? "vh" : "vw";
  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      if (boardState[i][j] == 0) empty.push([i,j])
    }
  }
  k = Math.floor(empty.length * Math.random());
  if (empty.length) boardState[empty[k][0]][empty[k][1]] = Math.random() * 10 / 9 > 1 ? 4 : 2;

  function colour (x) {
    for (var col = [,,], h = 0; h < 3; h++) col[h] = Math.round([0, 0, 255][h] * x / 16 + [255, 0, 0][h] * (16 - x) / 16);
    return "rgb(" + col[0] + "," + col[1] + "," + col[2] + ")"
  }
  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      $("div#cell" + i + j).textContent = boardState[i][j] ? boardState[i][j] : ""
      $("div#cell" + i + j).style.backgroundColor = boardState[i][j] ? colour(Math.log2(boardState[i][j])) : "#999";
      len = $("div#cell" + i + j).textContent.length;
      if (len > 2) $("div#cell" + i + j).style.fontSize = "calc(40" + vunit + "/" + len + " - 3rem/" + len + ")";
      else $("div#cell" + i + j).style.fontSize = "calc(20" + vunit + " - 1.5rem)"
    }
  }
}

function doGameOver() {
  gameover = true;
  drum.appendVal({score, moves}).then(updateHS);
  ws.send(JSON.stringify({msg: "validate", validate: {score, moves}}));
  $("span#again").classList.remove("hide")
}

function doStartGame() {
  gameover = false;
  boardState = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]];
  score = 0;
  moves = 0;
  updateBoard();
  updateBoard();
  $("span#again").classList.add("hide")
}

function transition(direction) {
  if (["ArrowLeft", "ArrowRight", "ArrowUp", "ArrowDown"].indexOf(direction) == -1 || gameover) return;
  var i, j, k, p, line, cell, full = true,
      hv = ["ArrowLeft", "ArrowRight"].indexOf(direction) == -1,
      fb = ["ArrowLeft", "ArrowUp"].indexOf(direction) == -1;

  for (i = 0; i < 4; i++) {
    p = -1;
    line = [];
    for (j = 0; j < 4; j++) {
      k = fb ? 3 - j : j;
      cell = hv ? boardState[k][i] : boardState[i][k];
      if (cell == 0) {
        full = false;
        continue
      }
      if (p == cell) {
        line.push(2 * p);
        score += 2 * p;
        p = -1
      } else {
        if (p != -1) line.push(p);
        p = cell
      }
    }
    if (p != -1) line.push(p);
    for (j = 0; (k = fb ? 3 - j : j), j < 4; j++) hv ?
      (testState[k][i] = line[j] || 0) :
      (testState[i][k] = line[j] || 0)
  }

  if (full) {
    var locked = true
    for (i = 0; i < 4; i++) {
      for (j = 0; j < 4; j++) {
        if (i != 3) boardState[i][j] == boardState[i + 1][j] && (locked = false);
        if (j != 3) boardState[i][j] == boardState[i][j + 1] && (locked = false)
      }
    }
    if (locked) {
      updateBoard();
      return doGameOver()
    }
  }

  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      if (testState[i][j] != boardState[i][j]) {
        boardState = testState;
        testState = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]];
        moves++;
        return updateBoard()
      }
    }
  }
}

// Init events
addEvents({
  "": {
    load: function () {
      var cell = $("div#cell").firstChild, row = $("div#row").firstChild, grid = $("div#grid"), nowrow;
      for (var i = 0; i < 4; i++) {
        nowrow = grid.appendChild(row.cloneNode(true));
        for (var j = 0; j < 4; j++) {
          nowrow.appendChild(cell.cloneNode(true)).setAttribute("id", "cell" + i + j)
        }
      }
      updateBoard();
      updateBoard()
    },

    keydown: function (e) {
      e.stopPropagation();
      transition(e.key)
    },

    resize: function () {
      var vunit = window.innerWidth / window.innerHeight > 1 ? "vh" : "vw";
      for (i = 0; i < 4; i++) {
        for (j = 0; j < 4; j++) {
          var len = $("div#cell" + i + j).textContent.length;
          if (len > 2) $("div#cell" + i + j).style.fontSize = "calc(40" + vunit + "/" + len + " - 3rem/" + len + ")";
          else $("div#cell" + i + j).style.fontSize = "calc(20" + vunit + " - 1.5rem)"
        }
      }
    },

    touchstart: function (e) {
      e.preventDefault();
      touch[0] = [e.touches[0].pageY, e.touches[0].pageX]
    },
    touchmove: function (e) {
      e.preventDefault();
      touch[1] = [e.touches[0].pageY, e.touches[0].pageX]
    },
    touchend: function (e) {
      if (e.timeStamp == touchTimestamp) return;
      else touchTimestamp = e.timeStamp;
      e.preventDefault();
      var theta = Math.atan2(touch[1][0] - touch[0][0], touch[1][1] - touch[0][1]) * 180 / Math.PI;
      if (theta < 45 && theta > -45) d = "ArrowRight";
      if (theta < 135 && theta > 45) d = "ArrowDown";
      if (theta < -135 || theta > 135) d = "ArrowLeft";
      if (theta < -45 && theta > -135) d = "ArrowUp";
      transition(d)
    }
  },
  "span#again": { "click touchend": doStartGame }
});
