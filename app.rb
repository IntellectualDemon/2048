require 'sinatra/base'
require 'erb'

module The2048
  class App < Sinatra::Base
    get '/' do
      send_file File.expand_path('2048.html', settings.public_folder)
    end
  end
end
