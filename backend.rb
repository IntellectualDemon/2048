require 'faye/websocket'
Faye::WebSocket.load_adapter 'thin'
require 'json'
require 'securerandom'

module The2048
  class Backend
    Player = Struct.new :ws, :name, :id, :secret
    Highscore = Struct.new :id, :score, :moves

    def initialize(app)
      @app = app
      @players = []
      @scores = []
    end

    def call(env)
      req = env["REQUEST_URI"]
      if Faye::WebSocket.websocket?(env)
        ws = Faye::WebSocket.new(env, nil, { ping: 30 })

        ws.on :open do |event|
          p ["WebSocket opened!", ws.object_id]
          ws.send(JSON.generate({:highscores => @scores.take(5).map {|c| {:score => c.score, :moves => c.moves} }}))
        end

        ws.on :message do |event|
          begin
            data = JSON.parse(event.data)
          rescue Exception => e
            p ["Message error!", e]
          end
          next unless data
          p ["A message!", data]

          player = @players.find {|c| c.ws == ws }
          if data["msg"] == "id" && data["id"]
            player = @players.find {|c| c.id == data["id"] }
            if player
              player.ws = ws
              p ["Update player list!", @players.map {|c| [c.name, c.id, c.secret] }]
            else
              ws.send(JSON.generate({:error => "ID not found"}))
            end
          elsif player && data["msg"] == "validate" && data["validate"]["moves"] && data["validate"]["score"]
            if validate data["validate"], player.id
              ws.send(JSON.generate({:highscores => @scores.take(5).map {|c| {:score => c.score, :moves => c.moves} }}))
            end
          elsif player.nil? && data["msg"] == "idrequest" && data["name"]
            player = Player.new(ws, nil, nil, nil)
            @players << player
            player.name = data["name"].to_s
            player.id = SecureRandom.uuid
            ws.send(JSON.generate({:name => player.name, :id => player.id}))
            player.secret = SecureRandom.hex(13)
            p ["Update player list!", @players.map {|c| [c.name, c.id, c.secret] }]
          end
        end

        ws.on :close do |event|
          p ["WebSocket closed!", ws.object_id, event.code, event.reason]
          player = @players.find {|c| c.ws == ws }
          player.ws = nil if player
          ws = nil
        end

        ws.rack_response
      else
        @app.call(env)
      end
    end

    def validate data, id
      @scores << Highscore.new(id, data["score"], data["moves"])
      @scores.sort! {|x, y| y.score <=> x.score }
      p ["Update score list!", @scores.take(5).map {|c| [c.id, c.score, c.moves]}]
      true
    end

  end
end
