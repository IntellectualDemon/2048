$stdout.sync = true
APP_ROOT = File.dirname(__FILE__)

require './app'
require './backend'
use The2048::Backend

# require 'rack/ssl-enforcer'
# use Rack::SslEnforcer

run The2048::App
